let loop = {
    data(){
        return {
            assignments: [
                { name: "Ovi", complete: false, id: "1"},
                { name: "Shuvo", complete: false, id: "2"},
                { name: "Rahat", complete: false, id: "3"},
            ]
        }
    },

    computed: {
        completedAssignments(){
            return this.assignments.filter(assignment => assignment.complete);
        },
        inProgress(){
            return this.assignments.filter(assignment => ! assignment.complete);
        }
    }
};

Vue.createApp(loop).mount("#app");